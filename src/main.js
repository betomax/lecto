import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLocalStorage from 'vue-ls'
import App from './App.vue'
import Login from './Login.vue'
import ProfileTeacher from './teacher/ProfileTeacher.vue'
import TeacherExercises from './teacher/Exercises.vue'
import TeacherNewExercise from './teacher/exercises/NewExercise.vue'
import TeacherEditExercise from './teacher/exercises/EditExercise.vue'
import TeacherStudents from './teacher/Students.vue'
import TeacherMe from './teacher/Me.vue'
import TeacherNewStudent from './teacher/students/new-student.vue'
import TeacherEditStudent from './teacher/students/edit-student.vue'
import TeacherRegister from './register.vue'
import ExerciseNewQuestion from './teacher/exercises/questions/NewQuestion.vue'
import ExerciseEditQuestion from './teacher/exercises/questions/EditQuestion.vue'

import ProfileStudent from './student/ProfileStudent.vue'
import StudentExercises from './student/Exercises.vue'
import StudentMe from './student/Me.vue'
import StudentExerciseDetail from './student/ExerciseDetail.vue'
import StudentExercisesResults from './student/ExercisesResults.vue'
import StudentResults from './student/Results.vue'

Vue.use(VueLocalStorage)
Vue.use(VueRouter)

const routes = [
  {path:'/', component:Login},
  {path:'/registro', component:TeacherRegister},
  {path:'/perfil', component:ProfileTeacher, children: [
        {path: 'ejercicios', component: TeacherExercises},
        {path: 'ejercicios/nuevo', component: TeacherNewExercise, children: [
              {path:'/', component: ExerciseNewQuestion}
        ]},
        {path: 'ejercicios/editar/:id', component: TeacherEditExercise, children:[
              {path:'/', name:'editExercise', component: ExerciseEditQuestion}
        ]},
        {path:'estudiantes', component: TeacherStudents},
        {path:':id', name:'teacher', component: TeacherMe},
        {path: 'estudiante/nuevo', component: TeacherNewStudent},
        {path: 'estudiante/editar/:id', name:'editStudent', component: TeacherEditStudent},
  ]},
  {path:'/perfils', component:ProfileStudent, children: [
        {path: 'ejercicios', component: StudentExercises},
        {path:'ejercicios/:id', name:'exercise', component: StudentExerciseDetail},
        {path:':id', name:'student', component: StudentMe},
        {path:'resultados/ejercicios', component: StudentExercisesResults},
        {path:'resultados/ejercicios/:idExercise', name:'results', component: StudentResults},
  ]}
]

const router = new VueRouter({
  routes,
  mode:'history'
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
